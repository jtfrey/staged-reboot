# staged-reboot

Staged en masse reboots on our Grid Engine clusters.

## Background

On a busy HPC cluster it's typically the case that getting all nodes rebooted
(e.g. to update their OS) requires scheduled downtime.  Many times this is not
a problem, but for a minor change -- like getting updated microcode on the
CPUs -- it would be great if we didn't have to interrupt user workflows as
destructively as with a hard reboot.

Some schedulers allow the sysadmin to set state on nodes such that no further
jobs will be scheduled until the node drains and gets rebooted.  Grid Engine
does not have such a feature.  So we had to implement it ourselves.

### Draining the nodes

Submitting a high-priority exclusive job to all the nodes was one option, but
it does not necessarily prevent Grid Engine from backfilling the node with
additional jobs (since we don't require wall time limits on all jobs).  To
allow running jobs to complete without scheduling additional jobs, all queue
instances on the node must be disabled.  Remembering the previous states is
critical to restoring service after reboot.

### Rebooting

We use `ipmitool` to effect a reboot of the node.

### Back online

After waiting for Grid Engine to miss enough state updates from the `sgeexecd`
on a node, we begin watching for `qhost` to report that the node is again
online.  At that point, the original queue states are restored and the node is
completed.

## The utility

```
usage: staged-reboot [-h] [--version] [--verbose] [--not-dryrun]
                     [--omit-offline-nodes] [--state-file <path>] [--restart]
                     [--node <hostname>] [--hostgroup <hostgroup>]
                     [--interval <seconds>] [--min-reboot-time <seconds>]
                     [<hostname> [<hostname> ...]]

Staged node reboots

positional arguments:
  <hostname>            add the given node to the list of nodes needing reboot
                        (see also --node/-n)

optional arguments:
  -h, --help            show this help message and exit
  --version, -V         show program version info
  --verbose, -v         increase level of output from program
  --not-dryrun, -D      actually perform all destructive commands (qmod,
                        reboot)
  --omit-offline-nodes, -o
                        remove any nodes that are initially offline from the
                        list of nodes needing reboot
  --state-file <path>, -s <path>
                        write state information to the given file path
  --restart, -r         used with the --state-file option to restart from the
                        state found in the file
  --node <hostname>, -n <hostname>
                        add the given node to the list of nodes needing reboot
  --hostgroup <hostgroup>, -g <hostgroup>
                        add all nodes in the given hostgroup to the list of
                        nodes needing reboot
  --interval <seconds>, -i <seconds>
                        seconds to sleep between state checks (default is 300)
  --min-reboot-time <seconds>, -m <seconds>
                        minimum seconds after a reboot that reboot complete
                        checks can start (default is 120)
```

### State file

The utility will produce and update a state file on each pass through the
state-advancement loop.  If the program were to crash or be killed prematurely
the loop can be restarted using the state file and the `--restart` option.

### Dry run

The utility defaults to dry-run mode:  no queue state changes or reboots are
actually performed.  The `--not-dryrun` flag is necessary to produce actual
state changes.

### Offline nodes

Any nodes that start out *offline* according to Grid Engine can be filtered
out.  If the node is offline then it will hypothetically be restarted manually
at some point in the future, so it may not be advisable to do so as part of
this staged procedure.